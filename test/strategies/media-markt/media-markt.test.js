jest.mock('axios');

const mediaMarkt = require('@core/use-case/strategies/media-markt');
const axios = require('axios');

describe('MEDIA MARKT strategy test suite', () => {
  it('should accept media-markt urls', () => {
    expect(mediaMarkt.accept({
      id: '333',
      url: 'https://www.mediamarkt.es/es/product/_consola-sony-ps5-825-gb-4k-hdr-blanco-1487016.html',
    })).toBe(true);
  });

  it('should not accept non-media-markt urls', () => {
    expect(mediaMarkt.accept({
      id: '333',
      url: 'https://www.pccomponentes.com/gigabyte-geforce-gt-1030-oc-2g-gddr5',
    })).toBe(false);
  });

  it('should resolve true when media-markt product is in stock', async () => {
    const resp = {
      data: '<html><head><meta property="og:availability" content="in stock"></head><body><div class="f-buyBox-availability f-buyBox-availabilityStatus-unavailable"></div></body></html>',
    };
    axios.get.mockResolvedValue(resp);
    const available = await mediaMarkt.checkStock({
      id: '333',
      url: 'https://www.mediamarkt.es/es/product/_consola-sony-ps5-825-gb-4k-hdr-blanco-1487016.html',
    });
    expect(available).toBe(true);
  });

  it('should resolve false when media-markt product is in stock', async () => {
    const resp = {
      data: '<html><head><meta property="og:availability" content="out of stock"></head><body><div class="f-buyBox-availability f-buyBox-availabilityStatus-available"></div></body></html>',
    };
    axios.get.mockResolvedValue(resp);
    const available = await mediaMarkt.checkStock({
      id: '333',
      url: 'https://www.mediamarkt.es/es/product/_consola-sony-ps5-825-gb-4k-hdr-blanco-1487016.html',
    });
    expect(available).toBe(false);
  });

  it('should resolve false when media-markt meta og:availability is missing', async () => {
    const resp = {
      data: '<html><head</head><body><div class="f-buyBox-availability f-buyBox-availabilityStatus-available"></div></body></html>',
    };
    axios.get.mockResolvedValue(resp);
    const available = await mediaMarkt.checkStock({
      id: '333',
      url: 'https://www.mediamarkt.es/es/product/_consola-sony-ps5-825-gb-4k-hdr-blanco-1487016.html',
    });
    expect(available).toBe(false);
  });
});
