jest.mock('axios');

const xtralife = require('@core/use-case/strategies/xtralife');
const axios = require('axios');

describe('XTRALIFE strategy test suite', () => {
  it('should accept xtralife urls', () => {
    expect(xtralife.accept({
      id: '333',
      url: 'https://www.xtralife.com/producto/mando-dualsense-ps5-mandos-oficial-sony/63262',
    })).toBe(true);
  });

  it('should not accept non-xtralife urls', () => {
    expect(xtralife.accept({
      id: '333',
      url: 'https://www.elcorteingles.es/videojuegos/A37046604/',
    })).toBe(false);
  });

  it('should resolve true when xtralife product is in stock', async () => {
    const resp = {
      data: {
        success: true,
        body: {
          id: 63262,
          name: 'Mando DualSense',
          disponibility: {
            disponibility: 'sell',
            signable: true,
            reservable: true,
            sign_price: 2,
            ttl: 0,
          },
        },
      },
    };
    axios.get.mockResolvedValue(resp);
    const available = await xtralife.checkStock({
      id: '333',
      url: 'https://www.xtralife.com/producto/mando-dualsense-ps5-mandos-oficial-sony/63262',
    });
    expect(available).toBe(true);
  });

  it('should resolve false when xtralife product is not in stock', async () => {
    const resp = {
      data: {
        success: true,
        body: {
          id: 63262,
          name: 'Mando DualSense',
          disponibility: {
            disponibility: 'reservation_maybe_out_of_stock',
            signable: true,
            reservable: true,
            sign_price: 2,
            ttl: 0,
          },
        },
      },
    };
    axios.get.mockResolvedValue(resp);
    const available = await xtralife.checkStock({
      id: '333',
      url: 'https://www.xtralife.com/producto/mando-dualsense-ps5-mandos-oficial-sony/63262',
    });
    expect(available).toBe(false);
  });
});
