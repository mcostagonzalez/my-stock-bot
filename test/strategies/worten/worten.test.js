jest.mock('axios');

const worten = require('@core/use-case/strategies/worten');
const axios = require('axios');

describe('WORTEN strategy test suite', () => {
  it('should accept worten urls', () => {
    expect(worten.accept({
      id: '333',
      url: 'https://www.worten.es/productos/consolas-juegos/playstation/consola/ps5/consola-ps5-825-gb-7196053',
    })).toBe(true);
  });

  it('should not accept non-worten urls', () => {
    expect(worten.accept({
      id: '333',
      url: 'https://www.elcorteingles.es/videojuegos/A37046604/',
    })).toBe(false);
  });

  it('should resolve true when worten product is in stock', async () => {
    const resp = {
      data: '<html><body><div class="w-product__actions-info__available" /></body></html>',
    };
    axios.get.mockResolvedValue(resp);
    const available = await worten.checkStock({
      id: '333',
      url: 'https://www.worten.es/productos/consolas-juegos/playstation/consola/ps5/consola-ps5-825-gb-7196053',
    });
    expect(available).toBe(true);
  });

  it('should resolve false when worten product is not in stock', async () => {
    const resp = {
      data: '<html><body><div class="w-product__actions-info__unavailable" /></body></html>',
    };
    axios.get.mockResolvedValue(resp);
    const available = await worten.checkStock({
      id: '333',
      url: 'https://www.worten.es/productos/consolas-juegos/playstation/consola/ps5/consola-ps5-825-gb-7196053',
    });
    expect(available).toBe(false);
  });
});
