jest.mock('axios');

const fnac = require('@core/use-case/strategies/fnac');
const axios = require('axios');

describe('FNAC strategy test suite', () => {
  it('should accept fnac urls', () => {
    expect(fnac.accept({
      id: '333',
      url: 'https://www.fnac.es/Consola-PlayStation-5-Videoconsola-Consola/a7724798',
    })).toBe(true);
  });

  it('should not accept non-fnac urls', () => {
    expect(fnac.accept({
      id: '333',
      url: 'https://www.pccomponentes.com/gigabyte-geforce-gt-1030-oc-2g-gddr5',
    })).toBe(false);
  });

  it('should resolve true when fnac product is in stock', async () => {
    const resp = {
      data: '<html><body><div class="f-buyBox-availability f-buyBox-availabilityStatus-unavailable"></div></body></html>',
    };
    axios.get.mockResolvedValue(resp);
    const available = await fnac.checkStock({
      id: '333',
      url: 'https://www.fnac.es/Consola-PlayStation-5-Videoconsola-Consola/a7724798',
    });
    expect(available).toBe(true);
  });

  it('should resolve false when fnac product is in stock', async () => {
    const resp = {
      data: '<html><body><div class="f-buyBox-availability f-buyBox-availabilityStatus-available"></div></body></html>',
    };
    axios.get.mockResolvedValue(resp);
    const available = await fnac.checkStock({
      id: '333',
      url: 'https://www.fnac.es/Consola-PlayStation-5-Videoconsola-Consola/a7724798',
    });
    expect(available).toBe(false);
  });
});
