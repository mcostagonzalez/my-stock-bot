jest.mock('axios');

const eci = require('@core/use-case/strategies/eci');
const axios = require('axios');

describe('ECI strategy test suite', () => {
  it('should accept eci urls', () => {
    expect(eci.accept({
      id: '333',
      url: 'https://www.elcorteingles.es/videojuegos/A37046604/',
    })).toBe(true);
  });

  it('should not accept non-eci urls', () => {
    expect(eci.accept({
      id: '333',
      url: 'https://www.pccomponentes.com/gigabyte-geforce-gt-1030-oc-2g-gddr5',
    })).toBe(false);
  });

  it('should resolve true when eci product is in stock', async () => {
    const resp = {
      data: '<html><body><input type="hidden" avalible=true id="product_not_avalible"/></body></html>',
    };
    axios.get.mockResolvedValue(resp);
    const available = await eci.checkStock({
      id: '333',
      url: 'https://www.elcorteingles.es/videojuegos/A37046604/',
    });
    expect(available).toBe(true);
  });

  it('should resolve false when eci product is in stock', async () => {
    const resp = {
      data: '<html><body><input type="hidden" avalible=false id="product_not_avalible"/></body></html>',
    };
    axios.get.mockResolvedValue(resp);
    const available = await eci.checkStock({
      id: '333',
      url: 'https://www.elcorteingles.es/videojuegos/A37046604/',
    });
    expect(available).toBe(false);
  });
});
