jest.mock('axios');

const pccomponentes = require('@core/use-case/strategies/pccomponentes');
const axios = require('axios');

describe('PCCOM strategy test suite', () => {
  it('should accept pccomponentes urls', () => {
    expect(pccomponentes.accept({
      id: '333',
      url: 'https://www.pccomponentes.com/gigabyte-geforce-gt-1030-oc-2g-gddr5',
    })).toBe(true);
  });

  it('should not accept non-pccomponentes urls', () => {
    expect(pccomponentes.accept({
      id: '333',
      url: 'https://www.elcorteingles.es/videojuegos/A37046604/',
    })).toBe(false);
  });

  it('should resolve true when pccomponentes product is in stock', async () => {
    const resp = {
      data: {
        storesAvailability: {
          murcia: true,
          madrid: false,
        },
        availability: {
          code: 1,
          status: 'inStock',
          realStock: -1,
          expected: '',
        },
      },
    };
    axios.get.mockResolvedValue(resp);
    const available = await pccomponentes.checkStock({
      id: '333',
      url: 'https://www.pccomponentes.com/gigabyte-geforce-gt-1030-oc-2g-gddr5',
    });
    expect(available).toBe(true);
  });

  it('should resolve false when pccomponentes product is in stock', async () => {
    const resp = {
      data: {
        storesAvailability: {
          murcia: false,
          madrid: false,
        },
        availability: {
          code: 4,
          status: 'outOfStock',
          realStock: -1,
          expected: '',
        },
      },
    };
    axios.get.mockResolvedValue(resp);
    const available = await pccomponentes.checkStock({
      id: '333',
      url: 'https://www.pccomponentes.com/gigabyte-geforce-gt-1030-oc-2g-gddr5',
    });
    expect(available).toBe(false);
  });
});
