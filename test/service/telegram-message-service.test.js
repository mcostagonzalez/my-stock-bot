jest.mock('telegram-bot-api');

const TG = require('telegram-bot-api');
const { telegramMessageService } = require('@core/service');

describe('Telegram API unit tests', () => {
  it('should call telegram test method', () => {
    TG.prototype.sendMessage = jest.fn();
    const { sendMessage } = TG.prototype;
    telegramMessageService.sendTgMessage('hi!');
    expect(sendMessage).toHaveBeenCalledTimes(1);
  });

  it('should not call telegram sendMessage if no message is issued', () => {
    TG.prototype.sendMessage = jest.fn();
    const { sendMessage } = TG.prototype;
    telegramMessageService.sendTgMessage();
    expect(sendMessage).toHaveBeenCalledTimes(0);
  });
});
