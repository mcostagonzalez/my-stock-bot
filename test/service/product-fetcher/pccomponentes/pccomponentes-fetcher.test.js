jest.mock('axios');

const pccomponentes = require('@core/service/product-fetcher/pccomponentes');
const axios = require('axios');

describe('PCComponentes fetcher test suite', () => {
  it('should accept pccomponentes category from the url', () => {
    expect(pccomponentes.accept({
      webpage: 'www.pccomponentes.com',
      urlSlug: '/listado',
    })).toBe(true);
  });

  it('should fetch all products from url filtering prices, one is overpriced', async () => {
    const resp = {
      data: '<a class="GTM-productClick" data-id="22222" data-stock-web="1" href="/asus-rog-strix-geforce-rtx-3060ti" data-price="39.99" /><a class="GTM-productClick" data-id="33333" data-stock-web="1" data-price="999.99" href="/asus-rog-strix-geforce-rtx-3060to" />',
    };
    axios.get.mockResolvedValue(resp);
    /**
     * @type {Product[]}
     */
    const products = await pccomponentes.fetchProducts({
      webpage: 'www.pccomponentes.com',
      urlSlug: '/listado',
      maxPrice: 500,
    });
    expect(products).toHaveLength(1);
  });

  it('should fetch all products from url filtering prices, both are ok', async () => {
    const resp = {
      data: '<a class="GTM-productClick" data-id="22222" data-stock-web="1" href="/asus-rog-strix-geforce-rtx-3060ti" data-price="39.99" /><a class="GTM-productClick" data-id="33333" data-stock-web="1" data-price="499.99" href="/asus-rog-strix-geforce-rtx-3060to" />',
    };
    axios.get.mockResolvedValue(resp);
    /**
     * @type {Product[]}
     */
    const products = await pccomponentes.fetchProducts({
      webpage: 'www.pccomponentes.com',
      urlSlug: '/listado',
      maxPrice: 500,
    });
    expect(products).toHaveLength(2);
  });

  it('should fetch all products from url filtering prices, none are ok', async () => {
    const resp = {
      data: '<a class="GTM-productClick" data-id="22222" data-stock-web="1" href="/asus-rog-strix-geforce-rtx-3060ti" data-price="3900.99" /><a class="GTM-productClick" data-id="33333" data-stock-web="1" data-price="4990.99" href="/asus-rog-strix-geforce-rtx-3060to" />',
    };
    axios.get.mockResolvedValue(resp);
    /**
     * @type {Product[]}
     */
    const products = await pccomponentes.fetchProducts({
      webpage: 'www.pccomponentes.com',
      urlSlug: '/listado',
      maxPrice: 500,
    });
    expect(products).toHaveLength(0);
  });
});
