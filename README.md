# README #

This repository contains my own stock bot implementation that I used to buy both my RTX 3070 and PS5.

## How do I get set up? ##

In order to setup this project you will need:

* NodeJS: 12+
* An internet connection: doh!
* The `azure-functions-core-tools` package installed.
* An Azure account where you can run Azure functions.
* Your own Telegram bot: [https://core.telegram.org/bots](https://core.telegram.org/bots) and a channel where it will publish messages.

## Running tests ##

Unit tests are run by using `npm run test` command.

If you want to try the service in your own computer, use the `npm start`.

### Who do I talk to? ###

* Repo owner or admin
