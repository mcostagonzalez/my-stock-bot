const { telegramMessageService, stockService } = require('../service');
const strategies = require('./strategies');

/**
 * Check stock on product
 * @param {Product} product The product to check stock
 */
async function runChecksOnProduct(product) {
  const isInStock = await strategies.checkStock(product);
  if (isInStock) {
    await telegramMessageService.sendTgMessage(`Product is in stock: ${product.url}`);
  }
}

/**
 * Tries to run the check stock logic, sending a message to the telegram group if there's an error
 * @param {Product} product The product to check stock
 */
async function tryRunChecksOnProduct(product) {
  try {
    await runChecksOnProduct(product);
  } catch (e) {
    await telegramMessageService.sendTgMessage(`Error checking for stock (${product.id}) : ${e}`);
  }
}

/**
 * Runs checks in every product
 */
async function runChecksInAllProducts() {
  const products = await stockService.getProducts();
  // eslint-disable-next-line no-restricted-syntax
  for (const product of products) {
    // eslint-disable-next-line no-await-in-loop
    await tryRunChecksOnProduct(product);
  }
}

module.exports = { runChecksInAllProducts };
