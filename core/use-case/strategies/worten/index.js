const axios = require('axios');
const cheerio = require('cheerio');

const { logger } = require('../../../util');

/**
 * Checks if the product is acceptable for this strategy
 * @param {Product} product The product we will apply this strategy
 */
function accept(product) {
  logger.debug(`WR: Checking if product ${product.url} is valid for strategy...`);
  if (product.url.startsWith('https://www.worten.es/')) { return true; }
  return false;
}

/**
 * Actually checks if the product has stock
 * @param {Product} product The product to check the stock
 */
async function checkStock(product) {
  const mmResponse = await axios.get(product.url);
  const $mmProduct = cheerio.load(mmResponse.data);
  if ($mmProduct('.w-product__actions-info__unavailable').length === 0) {
    return true;
  }
  return false;
}

module.exports = {
  accept,
  checkStock,
};
