const axios = require('axios');

const { logger } = require('../../../util');

const pccomponentesUrl = 'https://www.pccomponentes.com/ajax_nc/articles/price_and_availability?idArticle=';

/**
 * Checks if the product is acceptable for this strategy
 * @param {Product} product The product we will apply this strategy
 */
function accept(product) {
  logger.debug(`PCC: Checking if product ${product.url} is valid for strategy...`);
  if (product.url.startsWith('https://www.pccomponentes.com/')) { return true; }
  return false;
}

/**
 * Actually checks if the product has stock
 * @param {Product} product The product to check the stock
 */
async function checkStock(product) {
  const response = await axios.get(pccomponentesUrl + product.id);

  if (
    response.data.availability.code === 1
    || response.data.availability.code === 2
  ) {
    logger.debug(`Product ${product.url} is available!!`);
    return true;
  }

  return false;
}

module.exports = {
  accept,
  checkStock,
};
