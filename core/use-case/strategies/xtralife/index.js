const axios = require('axios');

const { logger } = require('../../../util');

const xtralifeApiUrl = 'https://api.xtralife.com/public-api/v1/sku?storefront_id=1&id=';

/**
 * Checks if the product is acceptable for this strategy
 * @param {Product} product The product we will apply this strategy
 */
function accept(product) {
  logger.debug(`XT: Checking if product ${product.url} is valid for strategy...`);
  if (product.url.startsWith('https://www.xtralife.com/')) { return true; }
  return false;
}

/**
 * Actually checks if the product has stock
 * @param {Product} product The product to check the stock
 */
async function checkStock(product) {
  const xtralifeResponse = await axios.get(xtralifeApiUrl + product.id);
  if (
    xtralifeResponse.data.body.disponibility.disponibility
    !== 'reservation_maybe_out_of_stock'
    && xtralifeResponse.data.body.disponibility.disponibility
    !== 'reservation_out_of_stock'
  ) {
    return true;
  }
  return false;
}

module.exports = {
  accept,
  checkStock,
};
