const { logger } = require('@core/util');

const eci = require('./eci');
const fnac = require('./fnac');
const mediaMarkt = require('./media-markt');
const pccomponentes = require('./pccomponentes');
const worten = require('./worten');
const xtralife = require('./xtralife');

const allStrategies = [eci, fnac, mediaMarkt, pccomponentes, worten, xtralife];

/**
 * Checks the stock for a product using the applicable strategy
 * @param {Product} product The product that we're checking the stock
 * @returns {boolean} if the product is in stock or not
 */
async function checkStock(product) {
  logger.debug(`Checking stock for ${product.url}`);
  let stockAvailable = false;

  // eslint-disable-next-line no-restricted-syntax
  for (const strategy of allStrategies) {
    if (strategy.accept(product)) {
      // eslint-disable-next-line no-await-in-loop
      stockAvailable = await strategy.checkStock(product);
    }
  }

  return stockAvailable;
}

module.exports = {
  checkStock,
};
