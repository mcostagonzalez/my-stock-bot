const axios = require('axios');
const cheerio = require('cheerio');

const { logger } = require('../../../util');

/**
 * Checks if the product is acceptable for this strategy
 * @param {Product} product The product we will apply this strategy
 */
function accept(product) {
  logger.debug(`MM: Checking if product ${product.url} is valid for strategy...`);
  if (product.url.startsWith('https://www.mediamarkt.es/')) { return true; }
  return false;
}

/**
 * Actually checks if the product has stock
 * @param {Product} product The product to check the stock
 */
async function checkStock(product) {
  const mmResponse = await axios.get(product.url);
  const $mmProduct = cheerio.load(mmResponse.data);
  if (
    $mmProduct('meta[property="og:availability"]').length > 0
    && $mmProduct('meta[property="og:availability"]').attr('content')
      !== 'out of stock'
  ) {
    return true;
  }
  return false;
}

module.exports = {
  accept,
  checkStock,
};
