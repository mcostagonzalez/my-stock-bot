const axios = require('axios');
const cheerio = require('cheerio');

const { logger } = require('../../../util');

/**
 * Checks if the product is acceptable for this strategy
 * @param {Product} product The product we will apply this strategy
 */
function accept(product) {
  logger.debug(`FN: Checking if product ${product.url} is valid for strategy...`);
  if (product.url.startsWith('https://www.fnac.es')) { return true; }
  return false;
}

/**
 * Actually checks if the product has stock
 * @param {Product} product The product to check the stock
 */
async function checkStock(product) {
  const fnacResponse = await axios.get(product.url);
  const $fnacProduct = cheerio.load(fnacResponse.data);

  if (
    $fnacProduct(
      '.f-buyBox-availability.f-buyBox-availabilityStatus-unavailable',
    ).length !== 0
  ) {
    return true;
  }

  return false;
}

module.exports = {
  accept,
  checkStock,
};
