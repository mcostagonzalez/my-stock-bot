const axios = require('axios');
const cheerio = require('cheerio');

const { logger } = require('../../../util');

/**
 * Checks if the product is acceptable for this strategy
 * @param {Product} product The product we will apply this strategy
 */
function accept(product) {
  logger.debug(`ECI: Checking if product ${product.url} is valid for strategy...`);
  if (product.url.startsWith('https://www.elcorteingles.es/')) { return true; }
  return false;
}

/**
 * Actually checks if the product has stock
 * @param {Product} product The product to check the stock
 */
async function checkStock(product) {
  logger.debug(`ECI: Checking availability of product ${product.url}...`);
  const eciResponse = await axios.get(product.url);
  const $eci = cheerio.load(eciResponse.data);
  if ($eci('#product_not_avalible').attr('avalible') === 'true') {
    logger.debug('ECI: Product is available');
    return true;
  }
  logger.debug('ECI: Product is not available :(');
  return false;
}

module.exports = {
  accept,
  checkStock,
};
