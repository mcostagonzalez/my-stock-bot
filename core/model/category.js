/**
 * Represents a category that we want to fetch products for
 * @typedef {object} Category
 * @property {string} webpage The webpage where we will fetch the products
 * @property {string} urlSlug The url slug that we'll use to fetch the content
 * @property {number} maxPrice The maximum price that we want to pay
 */
