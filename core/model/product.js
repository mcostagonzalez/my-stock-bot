/**
 * Represents a product that we want to check stock on
 * @typedef {object} Product
 * @property {string} id The product id
 * @property {string} url The product url
 */
