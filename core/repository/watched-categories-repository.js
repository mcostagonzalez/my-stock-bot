/**
 * The static array of categories to watch
 * @type {Category[]}
 */
const categories = [
  {
    webpage: 'www.pccomponentes.com',
    urlSlug: '/listado/ajax?idFilters%5B%5D=7793&idFilters%5B%5D=2756&page=0&order=price-asc&gtmTitle=Tarjetas%20Gr%C3%A1ficas%20Nvidia%20GeForce%20RTX%203060%20Series&idFamilies%5B%5D=6',
    maxPrice: 525,
  },
  {
    webpage: 'www.pccomponentes.com',
    urlSlug: '/listado/ajax?page=2&idFilters%5B%5D=7793&idFilters%5B%5D=2756&page=0&order=price-asc&gtmTitle=Tarjetas%20Gr%C3%A1ficas%20Nvidia%20GeForce%20RTX%203060%20Series&idFamilies%5B%5D=6',
    maxPrice: 525,
  },
];

/**
 * Returns the watched categories
 * @returns {Category[]} The array of watched categories
 */
function getCategories() {
  return categories;
}

module.exports = {
  getCategories,
};
