/**
 * A hardcoded products list
 */
const products = [
  {
    id: 'A37046604',
    url: 'https://www.elcorteingles.es/videojuegos/A37046604/',
  },
  {
    id: '322526',
    url: 'https://www.pccomponentes.com/sony-playstation-5',
  },
  {
    id: '1487016',
    url: 'https://www.mediamarkt.es/es/product/_consola-sony-ps5-825-gb-4k-hdr-blanco-1487016.html',
  },
  {
    id: '62204',
    url: 'https://www.xtralife.com/producto/ps5-consola-ps5-estandar/62204',
  },
  {
    id: '7196053',
    url: 'https://www.worten.es/productos/consolas-juegos/playstation/consola/ps5/consola-ps5-825-gb-7196053',
  },
];

/**
 * Returns the list of products to check stock
 * @returns {Product[]} The list of checkable stock products
 */
function fetchProducts() {
  return products;
}

module.exports = {
  fetchProducts,
};
