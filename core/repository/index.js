const watchedProductsRepository = require('./watched-products-repository');
const watchedCategoriesRepository = require('./watched-categories-repository');

module.exports = {
  watchedCategoriesRepository,
  watchedProductsRepository,
};
