const TG = require('telegram-bot-api');

const api = new TG({
  token: process.env.TELEGRAM_TOKEN,
});

/**
 * Sends message to Telegram group
 * @param {string} message The message
 */
async function sendTgMessage(message) {
  if (message) {
    await api.sendMessage({
      chat_id: process.env.TELEGRAM_CHAT,
      text: message,
    });
  }
}

module.exports = {
  sendTgMessage,
};
