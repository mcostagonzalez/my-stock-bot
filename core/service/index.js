const telegramMessageService = require('./telegram-message-service');
const stockService = require('./stock-service');

module.exports = {
  stockService,
  telegramMessageService,
};
