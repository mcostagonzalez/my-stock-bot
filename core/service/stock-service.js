const { watchedProductsRepository } = require('../repository');
const productFetcher = require('./product-fetcher');

/**
 * Retrieves the products that we will check stock for
 * @returns {Product[]} The products
 */
async function getProducts() {
  let products = watchedProductsRepository.fetchProducts();
  products = products.concat(await productFetcher.fetchProducts());
  return products;
}

module.exports = {
  getProducts,
};
