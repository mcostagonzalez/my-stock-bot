const { watchedCategoriesRepository } = require('../../repository');
const pccomponentesFetcher = require('./pccomponentes');
const { logger } = require('../../util');

const allFetchers = [pccomponentesFetcher];

/**
 * Fetches the products for the category
 * @param {Category} category The category
 */
async function fetchCategoryProducts(category) {
  let products = [];
  // eslint-disable-next-line no-restricted-syntax
  for (const fetcher of allFetchers) {
    if (fetcher.accept(category)) {
      // eslint-disable-next-line no-await-in-loop
      const fetchedProducts = await fetcher.fetchProducts(category);
      products = products.concat(fetchedProducts);
    }
  }
  return products;
}

/**
 * Tries to fetch a category products
 * @param {Category} category The category to fetch products
 */
async function tryFetchCategoryProducts(category) {
  let products = [];

  try {
    products = await fetchCategoryProducts(category);
  } catch (e) {
    logger.error(e);
  }

  return products;
}

/**
 * Fetches all products from categories
 * @returns {Product[]} The array of products fetched
 */
async function fetchProducts() {
  let products = [];
  const categories = watchedCategoriesRepository.getCategories();
  // eslint-disable-next-line no-restricted-syntax
  for (const category of categories) {
    // eslint-disable-next-line no-await-in-loop
    const fetchedProducts = await tryFetchCategoryProducts(category);
    products = products.concat(fetchedProducts);
  }
  return products;
}

module.exports = {
  fetchProducts,
};
