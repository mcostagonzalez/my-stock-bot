const axios = require('axios');
const cheerio = require('cheerio');

const { logger } = require('../../../util');

/**
 * Checks if the fetcher is valid for the url
 * @param category {Category} The category to check
 * @returns {boolean} If the fetcher applies or not
 */
function accept(category) {
  return category.webpage === 'www.pccomponentes.com';
}

/**
 * Fetches all pccomponentes products following the category
 * @param category {Category} The category
 * @returns {Product[]} The array of fetched products
 */
async function fetchProducts(category) {
  logger.debug(`PCComponentes: Fetching products at slug> ${category.urlSlug}...`);

  /** @type {Product[]} */
  const products = [];
  const pccResponse = await axios.get(`https://www.pccomponentes.com${category.urlSlug}`);
  const $pcc = cheerio.load(pccResponse.data);

  const productLinks = $pcc('a.GTM-productClick');

  for (let i = 0; i < productLinks.length; i += 1) {
    const price = parseFloat(productLinks[i].attribs['data-price']);
    if ((productLinks[i].attribs['data-stock-web'] === '1' || productLinks[i].attribs['data-stock-web'] === '2') && price < category.maxPrice) {
      products.push({
        id: productLinks[i].attribs['data-id'],
        url: `https://www.pccomponentes.com${productLinks[i].attribs.href}`,
      });
    }
  }

  return products;
}

module.exports = {
  accept,
  fetchProducts,
};
