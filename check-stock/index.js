require('module-alias/register');
const checkStockService = require('@core/use-case/check-stock-use-case');

async function triggerFunction() {
  await checkStockService.runChecksInAllProducts();
}

module.exports = triggerFunction;
